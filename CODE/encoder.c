#include "encoder.h"



void encoder_init(void)
{
	tim_counter_init(TIM_3,ENCODER_LSB);
	gpio_init(B5, GPI, 0, GPI_PULL_UP);
}

uint16 get_count(void)
{
	int16 encoder_count = 0;
	encoder_count = tim_counter_get_count(TIM_3);
	tim_counter_rst(TIM_3);
	if(gpio_get(B5) == 0)
	{
		encoder_count = -encoder_count;
	}
	return encoder_count;
}


uint16 filter(void) 
{ 
	static uint16 value_buff[4]; 
	static uint16 i=0; 
	uint16 count; 
	uint16 sum=0; 
	value_buff[i++]=get_count(); 
	if(i==4) 
			i=0; 
	for(count=0;count<4;count++)
	{
			sum+=value_buff[count]; 
	}

	return sum/4; 
} 













