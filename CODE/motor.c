#include "motor.h"

void Motor_Init(void)
{
	pwm_init(PWM_TIM, PWM_CH1, 1000, 0);					// PWM 通道1 初始化频率1KHz 占空比初始为0
	gpio_init(DIR_CH1, GPO, GPIO_HIGH, GPO_PUSH_PULL);		//初始化电机方向输出引脚			
	tim_counter_init(ENCODER1_TIM, ENCODER1_PLUS);			//初始化编码器采值引脚及定时器
	gpio_init(ENCODER1_DIR, GPI, GPIO_LOW, GPI_PULL_UP);	//初始化编码器方向端口
}

void Motor_Control(int duty)
{
	if(duty >= 0)																// 正转
		{
			pwm_duty_updata(PWM_TIM, PWM_CH1, duty*(PWM_DUTY_MAX/100));	 		// 计算占空比
			gpio_set(DIR_CH1,0);
		}
		else																		// 反转
		{
			pwm_duty_updata(PWM_TIM, PWM_CH1, -duty*(PWM_DUTY_MAX/100));	 
			gpio_set(DIR_CH1,1);
		}
}

int16_t Encoder_Get(void)
{
	int16_t encoder = 0;
	
	encoder = tim_counter_get_count(ENCODER1_TIM);
	tim_counter_rst(ENCODER1_TIM);
	
	if(gpio_get(ENCODER1_DIR))
	{
		encoder = -encoder;
	}
	else
	{
		encoder = encoder;
	}
	
	return encoder;
}










