#ifndef __SD5_H
#define __SD5_H
#include "headfile.h"

#define Servo_PWM_TIM				TIM_2
#define Servo_PWM_CH1				TIM_2_CH1_A15

//100HZ控制频率
#define Servo_Min     4900    //舵机左转极限值
#define Servo_Max    7700     //舵机右转极限值
#define Servo_Middle_Num   6350    //舵机中值

void Servo_init(void);
void Servo_out(float turn_angle, float true_angle);
void Servo__Stright_PID_Init(void);
void Servo_Turn_PID_Init(void);



#endif












