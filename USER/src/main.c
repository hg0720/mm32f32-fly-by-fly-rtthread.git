/*********************************************************************************************************************
* COPYRIGHT NOTICE
* Copyright (c) 2019,逐飞科技
* All rights reserved.
* 技术讨论QQ群：一群：179029047(已满)  二群：244861897
*
* 以下所有内容版权均属逐飞科技所有，未经允许不得用于商业用途，
* 欢迎各位使用并传播本程序，修改内容时必须保留逐飞科技的版权声明。
*
* @file				main
* @company			成都逐飞科技有限公司
* @author			逐飞科技(QQ3184284598)
* @version			查看doc内version文件 版本说明
* @Software			IAR 8.3 or MDK 5.24
* @Target core		MM32F3277
* @Taobao			https://seekfree.taobao.com/
* @date				2021-02-22
********************************************************************************************************************/

#include "headfile.h"
// **************************** 宏定义 ****************************
#define SECTION_INDEX		FLASH_SECTION_127										// 最后一个扇区
#define PAGE_INDEX			FLASH_PAGE_3											// 最后一个页存储操场数据
//#define PAGE_INDEX			FLASH_PAGE_2												// 倒数第二个页存储测试数据
#define DATA_SIZE			1024/sizeof(uint32)										// 32bit数据存储 缓冲就是1024/4大小
	
#define Point_Num       22

#define Turn_Point1     1
#define Turn_Point2			12

#define Stright_Point1   10
#define Stright_Point2   20

#define Stright_Speed    100
#define Turn_Speed       100
// **************************** 宏定义 ****************************

// **************************** 变量定义 ****************************

float pitch,roll,yaw; 		//欧拉角
short aacx,aacy,aacz;			//加速度传感器原始数据

//存储经纬度信息
double test_latitude[Point_Num];   //纬度
double test_longitude[Point_Num];  //经度

float F_Angle=360;        //初始角度
float Turn_set;      			//设定角度
extern PID_TypeDef Servo_PID;  //舵机PID

uint32 data_buffer[DATA_SIZE];
uint8 *data_point = (uint8 *)data_buffer;
int i,j;

static rt_sem_t Car_sem = RT_NULL;							// 创建指向信号量的指针
static rt_sem_t MPU_sem = RT_NULL;							// 创建指向信号量的指针

uint8 stop_flag;
// **************************** 变量定义 ****************************
// **************************** 代码区域 ****************************
float ex_int2float(unsigned long value)       //长整型转换为浮点型
{
//	定义联合体
	union{
		float float_value;
		unsigned long int_value;
	}c;
//	存储数据转换
	c.int_value = value;//以长整型存入联合体
	return c.float_value;//以浮点型返回
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      初始化线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void init_entry(void *parameter)
{
	IIC_Init();
	
	gps_init();
	
	uart_init(UART_4, 9600, UART4_TX_C10, UART4_RX_C11);
	uart_rx_irq(UART_4, 1);
	
	lcd_init();
	
	lcd_showstr(0, 1, "RUNING");
	lcd_showstr(0, 2, "STATE:");
	lcd_showstr(0, 3, "LATI:");
	lcd_showstr(0, 4, "LONG:");
	lcd_showstr(0, 5, "ANGLE:");
	lcd_showstr(0, 6, "FLASH WAIT");
	lcd_showstr(0, 7, "COUNT:");
	lcd_showstr(0, 8, "DIST:");
	lcd_showstr(0, 9, "ASET:");
	
	while(MPU_Init());					//初始化MPU6050
	rt_kprintf("mpu6050 Init.\r\n");
	while(mpu_dmp_init())
	{
		systick_delay_ms(200);
		rt_kprintf("dmp Init err.\r\n");
		lcd_showstr(0, 5, "ANGLE:NULL");
	}
	rt_kprintf("dmp Init.\r\n");
	
	memset(data_buffer, 0xff, sizeof(data_buffer));								// 给缓冲区填充数据

//循迹三十个点
	if(flash_check(SECTION_INDEX, PAGE_INDEX))										// 检查Flash是否有数据
	{
		flash_page_read(SECTION_INDEX, PAGE_INDEX, data_buffer, DATA_SIZE);			// 读出来已有的数据
		for(i=0,j=0; i<Point_Num; i++, j+=2)
		{
			test_latitude[i] = ex_int2float(data_buffer[j]);
		}
		for(i=0,j=1; i<Point_Num; i++, j+=2)
		{
			test_longitude[i] = ex_int2float(data_buffer[j]);
		}
		lcd_showstr(0, 6, "FLASH  YES");	
		
		//打印经纬度数据
		for(i=0; i<Point_Num; i++)
		{
			static int count=1;
			rt_kprintf("POINT %d : %f, %f\r\n",count, test_latitude[i], test_longitude[i]);
			count++;
		}
		//等待GPS返回数据
		while(gps_tau1201_flag == 0 || gps_tau1201.state == 0)
		{
			systick_delay_ms(1);
		}
		if(gps_tau1201_flag)
		{
			gps_tau1201_flag = 0;
			gps_data_parse();           //开始解析数据
			lcd_showuint8(50, 2, gps_tau1201.state);            //输出当前定位有效模式 1：定位有效  0：定位无效
			lcd_showfloat(45, 3, gps_tau1201.latitude, 3, 3);   //输出纬度信息
			lcd_showfloat(45, 4, gps_tau1201.longitude, 3, 3);  //输出经度信息
//			F_Angle = get_two_points_azimuth(gps_tau1201.latitude, gps_tau1201.longitude,test_latitude[0], test_longitude[0]);
		}
		
		Servo_init();
		Servo__Stright_PID_Init();
		Turn_set=0;

		Motor_Init();
		Motor_Control(Stright_Speed);
	
		rt_sem_release(Car_sem);
		rt_sem_release(MPU_sem);
	}
	else
	{
		lcd_showstr(0, 6, "FLASH ERROR");
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      角度读取线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void MPU_thread_entry(void *parameter)
{
	static rt_err_t result;
	result = rt_sem_take(MPU_sem, RT_WAITING_FOREVER);
	while(result == RT_EOK)
	{
		if(mpu_dmp_get_data(&pitch,&roll,&yaw)==0)
		{
			if(yaw < 0)
			{
				yaw = yaw + 360;
			}
			lcd_showfloat(45, 5, yaw, 3, 3);
			if((roll >= 30) || (roll <= -30) || (pitch >= 30) || (pitch <= -30))
			{
				Motor_Control(0);
				pwm_disable(Servo_PWM_TIM);
			}
		}
		else
		{
			rt_kprintf("Read Err.\r\n");
		}			
		systick_delay_ms(10);
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      角度控制线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void Angle_thread_entry(void *parameter)
{
	float value;
	while(1)
	{
		value = Servo_Middle_Num - PID_Loc(Turn_set, yaw, &Servo_PID) ;
		if (value >= Servo_Max)                  //限制幅值
		{
				value = Servo_Max;
		}
		else if (value <= Servo_Min)            //限制幅值
		{
				value = Servo_Min;
		}
		pwm_duty_updata(Servo_PWM_TIM, Servo_PWM_CH1, value); //设置舵机转角	
		systick_delay_ms(10);
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      GPS处理线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void GPS_thread_entry(void *parameter)
{
	while(1)
	{
		if(gps_tau1201_flag)
		{
			gps_tau1201_flag = 0;
			gps_data_parse();           //开始解析数据
			lcd_showuint8(50, 2, gps_tau1201.state);            //输出当前定位有效模式 1：定位有效  0：定位无效
			lcd_showfloat(45, 3, gps_tau1201.latitude, 3, 3);   //输出纬度信息
			lcd_showfloat(45, 4, gps_tau1201.longitude, 3, 3);  //输出经度信息
		}
		systick_delay_ms(10);
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      遥控刹车处理线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void Stop_thread_entry(void *parameter)
{
	while(1)
	{
		if(stop_flag == 1)
		{
			pwm_disable(PWM_TIM);
			pwm_disable(Servo_PWM_TIM);
			lcd_showstr(0, 1, "STOP!      ");
		}
		systick_delay_ms(10);
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      小车控制线程入口
//  @param      parameter   参数
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
void Car_thread_entry(void *parameter)
{
	static rt_err_t result;			
	static int count=0;
	float distance;        //两点距离
	float azimuth_N;
	float last_turn=0;
	
	result = rt_sem_take(Car_sem, RT_WAITING_FOREVER);
	while(result == RT_EOK)
	{
		distance = get_two_points_distance(gps_tau1201.latitude, gps_tau1201.longitude, test_latitude[count], test_longitude[count]);
		azimuth_N = get_two_points_azimuth(gps_tau1201.latitude, gps_tau1201.longitude,test_latitude[count], test_longitude[count]);
		//两点距离小于一米则寻下一个点
		if ((distance <= 2))
		{
			count++;
		}
		else	if(((Turn_set - last_turn >= 35) || (Turn_set - last_turn <= -35)) && (distance <= 10))
		{
			Turn_set = last_turn;
			count++;
		}
		
		else
		{
			last_turn = Turn_set;
			Turn_set =  F_Angle - azimuth_N;
		}
		
		if((count == Turn_Point1) || (count == Turn_Point2))
		{
			Servo_Turn_PID_Init();
			Motor_Control(Turn_Speed);
		}
		if((count == Stright_Point1) || (count == Stright_Point2))
		{
			Servo__Stright_PID_Init();
			Motor_Control(Stright_Speed);
		}
		
		if(count == Point_Num)
		{
			Motor_Control(0);
			pwm_disable(Servo_PWM_TIM);
		}
		
		lcd_showint8(45, 7, count);                    //输出导航点计数
		lcd_showfloat(45, 8, distance, 3, 3);  //输出两点距离信息
		lcd_showfloat(45, 9, Turn_set, 3, 3);  //输出设定角度信息
		systick_delay_ms(10);
	}
}

//-------------------------------------------------------------------------------------------------------------------
//  @brief      线程创建以及启动
//  @param      void        空
//  @return     void
//  Sample usage:
//-------------------------------------------------------------------------------------------------------------------
static rt_uint8_t 	Angle_thread_stack[1024];  //线程栈数组
struct rt_thread 		Angle_thread_thread;        //线程控制块

static rt_uint8_t 	MPU_thread_stack[1024];  //线程栈数组
struct rt_thread 		MPU_thread_thread;        //线程控制块

static rt_uint8_t 	GPS_thread_stack[1024];  //线程栈数组
struct rt_thread 		GPS_thread_thread;        //线程控制块

static rt_uint8_t 	Car_thread_stack[1024];  //线程栈数组
struct rt_thread 		Car_thread_thread;        //线程控制块

static rt_uint8_t 	Stop_thread_stack[1024];  //线程栈数组
struct rt_thread 		Stop_thread_thread;        //线程控制块

int dynamic_thread(void)
{
		Car_sem = rt_sem_create("Car_semaphore", 0 ,RT_IPC_FLAG_FIFO);
		MPU_sem = rt_sem_create("MPU_semaphore", 0 ,RT_IPC_FLAG_FIFO);
    //线程控制块指针
    rt_thread_t tid1;	
		rt_err_t res;
    //创建动态线程
    tid1 = rt_thread_create("init",             //线程名称
            init_entry,                         //线程入口函数
            RT_NULL,                            //线程参数
            1024,                               //1024个字节的栈空间
            1,                                  //线程优先级为1，数值越小，优先级越高，0为最高优先级。
                                                //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
            5);                                 //时间片为5
	
    if(tid1 != RT_NULL)                     //线程创建成功
    {
        rt_kprintf("init thread create OK.\n");
        rt_thread_startup(tid1);            //运行该线程
    }
    else                                    //线程创建失败
    {
        rt_kprintf("init thread create ERROR.\n");
        return 1;
    }
		
	  //创建静态线程
    res = rt_thread_init(
                &MPU_thread_thread,                //线程控制块
                "MPU",                             //线程名称
                MPU_thread_entry,                  //线程入口函数
                RT_NULL,                           //线程参数
                MPU_thread_stack,                  //栈的起始地址
                sizeof(MPU_thread_stack),          //栈大小
                2,                                 //线程优先级为2，数值越小，优先级越高，0为最高优先级。
                                                   //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
                10                                 //时间片为10
            );

    if(res == RT_EOK)                                       //线程创建成功
    {
        rt_kprintf("MPU thread create OK\n");
        rt_thread_startup(&MPU_thread_thread);                 //运行该线程

    }
    else                                                    //线程创建失败
    {
        rt_kprintf("MPU thread create ERROR\n");
        return 2;
    }
		
    //创建静态线程
    res = rt_thread_init(
                &Angle_thread_thread,                 //线程控制块
                "Angle",                              //线程名称
                Angle_thread_entry,                   //线程入口函数
                RT_NULL,                              //线程参数
                Angle_thread_stack,                   //栈的起始地址
                sizeof(Angle_thread_stack),           //栈大小
                3,                                    //线程优先级为3，数值越小，优先级越高，0为最高优先级。
                                                      //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
                10                                    //时间片为10
            );

    if(res == RT_EOK)                                       //线程创建成功
    {
        rt_kprintf("Angle thread create OK\n");
        rt_thread_startup(&Angle_thread_thread);            //运行该线程

    }
    else                                                    //线程创建失败
    {
        rt_kprintf("Angle thread create ERROR\n");
        return 2;
    }
		
		//创建静态线程
    res = rt_thread_init(
                &GPS_thread_thread,                   //线程控制块
                "GPS",                                //线程名称
                GPS_thread_entry,                     //线程入口函数
                RT_NULL,                              //线程参数
                GPS_thread_stack,                     //栈的起始地址
                sizeof(GPS_thread_stack),             //栈大小
                2,                                    //线程优先级为2，数值越小，优先级越高，0为最高优先级。
                                                      //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
                20                                    //时间片为20
            );

    if(res == RT_EOK)                                       //线程创建成功
    {
        rt_kprintf("GPS thread create OK\n");
        rt_thread_startup(&GPS_thread_thread);              //运行该线程

    }
    else                                                    //线程创建失败
    {
        rt_kprintf("GPS thread create ERROR\n");
        return 2;
    }
		
		//创建静态线程
    res = rt_thread_init(
                &Stop_thread_thread,                 //线程控制块
                "Stop",                              //线程名称
                Stop_thread_entry,                   //线程入口函数
                RT_NULL,                              //线程参数
                Stop_thread_stack,                   //栈的起始地址
                sizeof(Stop_thread_stack),           //栈大小
                2,                                    //线程优先级为3，数值越小，优先级越高，0为最高优先级。
                                                      //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
                10                                    //时间片为10
            );

    if(res == RT_EOK)                                       //线程创建成功
    {
        rt_kprintf("Stop thread create OK\n");
        rt_thread_startup(&Stop_thread_thread);            //运行该线程

    }
    else                                                    //线程创建失败
    {
        rt_kprintf("Stop thread create ERROR\n");
        return 2;
    }
		
		//创建静态线程
    res = rt_thread_init(
                &Car_thread_thread,                  //线程控制块
                "Car",                               //线程名称
                Car_thread_entry,                    //线程入口函数
                RT_NULL,                             //线程参数
                Car_thread_stack,                    //栈的起始地址
                sizeof(Car_thread_stack),            //栈大小
                3,                                   //线程优先级为3，数值越小，优先级越高，0为最高优先级。
                                                     //可以通过修改rt_config.h中的RT_THREAD_PRIORITY_MAX宏定义(默认值为8)来修改最大支持的优先级
                10                                   //时间片为10
            );

    if(res == RT_EOK)                                       //线程创建成功
    {
        rt_kprintf("Car thread create OK\n");
        rt_thread_startup(&Car_thread_thread);              //运行该线程

    }
    else                                                    //线程创建失败
    {
        rt_kprintf("Car thread create ERROR\n");
        return 2;
    }

    return 0;
}

//使用INIT_APP_EXPORT宏自动初始化，也可以通过在其他线程内调用dynamic_thread_example函数进行初始化
INIT_APP_EXPORT(dynamic_thread);     //应用初始化

int main(void)
{
	//此处编写用户代码(例如：外设初始化代码等)
	gpio_init(H2, GPO, 0, GPO_PUSH_PULL);
	//此处编写用户代码(例如：外设初始化代码等)

	while(1)
	{
		rt_thread_mdelay(500);
    gpio_toggle(H2);
	}
}
// **************************** 代码区域 ****************************
void lora_callback (void)
{
	uint8 dat;
	uart_query(UART_4,&dat);
	stop_flag = 1;
}
	
