#ifndef __IIC_H
#define __IIC_H
#include "stdio.h"
#include "string.h"
#include "common.h"
#include "headfile.h"
#include "mpu6050.h"
 
#define SDA_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_15)//SDA IIC接口的时钟信号
#define SDA_Set() GPIO_SetBits(GPIOB,GPIO_Pin_15)

#define SCL_Clr() GPIO_ResetBits(GPIOB,GPIO_Pin_13)//SCL IIC接口的数据信号
#define SCL_Set() GPIO_SetBits(GPIOB,GPIO_Pin_13)

 		     
#define CMD  0	//写命令
#define DATA 1	//写数据
 
void IIC_Init(void);
void IIC_ACK(void);
void IIC_NACK(void);
u8 IIC_wait_ACK(void);
void IIC_Start(void);
void IIC_Stop(void);
void IIC_SendByte(u8 byte);
u8 IIC_RcvByte(void);
u8 IIC_Read_Byte(u8 ack);


u8 MPU_Write_Byte(u8 reg,u8 data);
u8 MPU_Read_Byte(u8 reg);
u8 MPU_Write_Len(u8 addr,u8 reg,u8 len,u8 *buf);
u8 MPU_Read_Len(u8 addr,u8 reg,u8 len,u8 *buf);
#endif

