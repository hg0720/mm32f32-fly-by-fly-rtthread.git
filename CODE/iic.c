#include "iic.h"

static uint16 iic_delay_time=100;		

static void iic_delay(void)
{
	uint16 delay_time;
	delay_time = iic_delay_time;
	while(delay_time--);
}

void IIC_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;

	RCC_AHBPeriphClockCmd(RCC_AHBENR_GPIOB, ENABLE);

	GPIO_StructInit(&GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin  =  GPIO_Pin_13|GPIO_Pin_15;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	GPIO_SetBits(GPIOB, GPIO_Pin_13|GPIO_Pin_15);
}

void SDA_IN(void)
{
    GPIO_InitTypeDef GPIO_InitStructer;
    GPIO_InitStructer.GPIO_Pin= GPIO_Pin_15;
    GPIO_InitStructer.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructer.GPIO_Mode=GPIO_Mode_IPU;
    GPIO_Init(GPIOB, &GPIO_InitStructer);
}

void SDA_OUT(void)
{
    GPIO_InitTypeDef GPIO_InitStructer;
    GPIO_InitStructer.GPIO_Pin= GPIO_Pin_15;
    GPIO_InitStructer.GPIO_Speed=GPIO_Speed_50MHz;
    GPIO_InitStructer.GPIO_Mode=GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructer);
}

void IIC_Start()
{
	SDA_OUT();
	SDA_Set();
	SCL_Set();
	iic_delay(); 
	SDA_Clr();
	iic_delay(); 
	SCL_Clr();
	iic_delay(); 
}

void IIC_Stop()
{
	SDA_OUT();
	SDA_Clr();
	iic_delay();
	SCL_Set();
	iic_delay();
	SDA_Set();
	iic_delay();
}


/*主机发送ACK*/
void IIC_ACK(void)
{
	SCL_Clr();
  SDA_OUT();
  SDA_Clr();
	iic_delay();
  SCL_Set();      
	iic_delay();	
  SCL_Clr();                        
}
 
/*主机不发送ACK*/
void IIC_NACK(void)
{
	SCL_Clr();
  SDA_OUT();
	SDA_Set();
	iic_delay();	
  SCL_Set();
	iic_delay();	
  SCL_Clr();                        
}

u8 IIC_wait_ACK(void)
{
	u8 Time=0;
	SDA_IN();
	SDA_Set();
	iic_delay();
	SCL_Set();
	iic_delay();
	while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15))
	{
			Time++;
			if(Time>250)
			{
					IIC_Stop();
					return 1;
			}
	}
	SCL_Clr();
	return 0;
}

void Write_IIC_Byte(unsigned char IIC_Byte)
{
	SDA_OUT();
	unsigned char i;
	unsigned char m,da;
	da=IIC_Byte;
	SDA_Clr();
	for(i=0;i<8;i++)		
	{
		m=da;
		m=m&0x80;
		if(m==0x80)
		{
			SCL_Set();
		}
		else SCL_Clr();
			da=da<<1;
		SDA_Set();
		SDA_Clr();
		}
}

/*******************************************************************
字节数据发送函数               
函数原型: void  SendByte(UCHAR c);
功能:将数据c发送出去,可以是地址,也可以是数据
********************************************************************/
void  IIC_SendByte(u8 byte)
{
	u8  BitCnt;
	SDA_OUT();
	SCL_Clr();
	for(BitCnt=0;BitCnt<8;BitCnt++)//要传送的数据长度为8位
	{
		if(byte&0x80) SDA_Set();//判断发送位
		else SDA_Clr(); 
		byte<<=1;
		iic_delay();
		SCL_Set();
		iic_delay();
		SCL_Clr();
		iic_delay();
	}
}
/*******************************************************************
 字节数据接收函数               
函数原型: UCHAR  RcvByte();
功能: 用来接收从器件传来的数据  
********************************************************************/    
u8 IIC_RcvByte(void)
{
  u8 retc;
  u8 BitCnt;
  retc=0; 
  SDA_IN();//置数据线为输入方式                   
  for(BitCnt=0;BitCnt<8;BitCnt++)
  {  
		SCL_Clr();	//置时钟线为低，准备接收数据位
		iic_delay();               
		SCL_Set();	//置时钟线为高使数据线上数据有效                
		retc=retc<<1;
		if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15)) retc |=1;//读数据位,接收的数据位放入retc中 
  }
  SCL_Clr();    
  return(retc);
}


u8 IIC_Read_Byte(u8 ack)
{
    u8 i,receive=0;
    SDA_IN();
    for(i=0;i<8;i++)
    {
        SCL_Clr();
        iic_delay();
        SCL_Set();
        receive<<=1;
        if(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15))receive++;
        iic_delay();
    }
    if(!ack) IIC_NACK();
    else IIC_ACK();
    return receive;
}














//IIC写一个字节 
//reg:      寄存器地址
//data:     数据
//返回值:  0,正常
//          其他,错误代码
u8 MPU_Write_Byte(u8 reg,u8 data)
{
    IIC_Start();
    IIC_SendByte((MPU_ADDR<<1)|0);//发送器件地址+写命令 
    if(IIC_wait_ACK())  //等待应答
    {
        IIC_Stop(); 
        return 1;       
    }
    IIC_SendByte(reg); //写寄存器地址
    IIC_wait_ACK();     //等待应答 
        IIC_SendByte(data);//发送数据
    if(IIC_wait_ACK())  //等待ACK
    {
        IIC_Stop();  
        return 1;        
    }        
    IIC_Stop();  
    return 0;
}

//IIC读一个字节 
//reg:寄存器地址 
//返回值:读到的数据

u8 MPU_Read_Byte(u8 reg)
{
    u8 res;
    IIC_Start();
    IIC_SendByte((MPU_ADDR<<1)|0);//发送器件地址+写命令 
    IIC_wait_ACK();//等待应答
    IIC_SendByte(reg);//写寄存器地址
    IIC_wait_ACK();//等待应答
    IIC_Start();
    IIC_SendByte((MPU_ADDR<<1)|1);//发送期间地址+读命令
    IIC_wait_ACK();//等待应答
    res=IIC_Read_Byte(0);//读取数据，发送nACK
    IIC_Stop();//产生一个停止条件
    return res;
}

//IIC连续写
//addr:器件地址
//reg: 寄存器地址
//len: 写入长度
//buf: 数据区
//返回值: 0,正常
//              其他，错误代码
u8 MPU_Write_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{
    u8 i;
    IIC_Start();
    IIC_SendByte((addr<<1)|0);//发送器件地址+写命令
    if(IIC_wait_ACK())//等待应答
    {
        IIC_Stop();
        return 1;
    }
    IIC_SendByte(reg);//写寄存器地址
    IIC_wait_ACK();//等待应答
    for(i=0;i<len;i++)
    {
        IIC_SendByte(buf[i]);//发送数据
        if(IIC_wait_ACK())//等待ACK
        {
            IIC_Stop();
            return 1;
        }
    }
    IIC_Stop();
    return 0;
}
//IIC连续读
//addr:器件地址
//reg:要读取的寄存器地址
//len:要读取得长度
//buf:读取到的数据存储区
//返回值: 0,正常
//              其他，错误代码
u8 MPU_Read_Len(u8 addr,u8 reg,u8 len,u8 *buf)
{
    IIC_Start();
    IIC_SendByte((addr<<1)|0);//发送器件地址+写命令
    if(IIC_wait_ACK())//等待应答
    {
        IIC_Stop();
        return 1;
    }
    IIC_SendByte(reg);//写寄存器地址
    IIC_wait_ACK();//等待应答
    IIC_Start();
    IIC_SendByte((addr<<1)|1);//发送器件地址+读命令
    IIC_wait_ACK();//等待应答
    while(len)
    {
        if(len==1) *buf=IIC_Read_Byte(0);//读数据，发送nACK
        else *buf=IIC_Read_Byte(1);//读数据，发送ACK
        len--;
        buf++;
    }
    IIC_Stop();//产生一个停止条件
    return 0;
}








