#ifndef __MOTOR_H
#define __MOTOR_H
#include "headfile.h"

#define MAX_DUTY			30				//最大占空比输出限制
#define PWM_TIM				TIM_5			//PWM定时器
#define PWM_CH1				TIM_5_CH1_A00	//PWM输出端口
#define DIR_CH1				A1				//电机方向输出端口

#define ENCODER1_TIM		TIM_3			//编码器定时器
#define ENCODER1_PLUS		TIM_3_ENC1_B04	//编码器计数端口
#define ENCODER1_DIR		B5				//编码器方向采值端口

void Motor_Init(void);
void Motor_Control(int duty);
int16_t Encoder_Get(void);

#endif





















