#ifndef __ENCODER_H
#define __ENCODER_H
#include "headfile.h"

#define ENCODER					TIM_3
#define ENCODER_LSB					TIM_3_ENC1_B04

void encoder_init(void);
uint16 get_count(void);
uint16 filter(void);

#endif






