#include "pid.h"
/************************************************
函数名称 ： PID_Loc
功    能 ： PID位置(Location)计算
参    数 ： SetValue ------ 设置值(期望值)
            ActualValue --- 实际值(反馈值)
            PID ----------- PID数据结构
返 回 值 ： PIDLoc -------- PID位置
作    者 ： strongerHuang
*************************************************/

float PID_Loc(float SetValue, float ActualValue, PID_TypeDef *PID)
{
	float err;
	float PWM;                                 //PWM输出
 
	err = SetValue - ActualValue;
	if(err <= -300)
	{
		err =  360 + err;
	}
	if(err >= 300)
	{
		err = err - 360;
	}
	PID->Ek = err;          //计算当前误差
	PID->LocSum += PID->Ek;                    //累计误差

	 PWM = PID->Kp * PID->Ek + (PID->Ki * PID->LocSum) + PID->Kd
	 * (PID->Ek1 - PID->Ek);                  //位置式 PID 控制

	PID->Ek1 = PID->Ek;                       //保存上一次偏差

	return PWM;
}

/************************************************
函数名称 ： PID_Inc
功    能 ： PID增量(Increment)计算
参    数 ： SetValue ------ 设置值(期望值)
            ActualValue --- 实际值(反馈值)
            PID ----------- PID数据结构
返 回 值 ： PIDInc -------- 本次PID增量(+/-)
作    者 ： strongerHuang
*************************************************/
float PID_Inc(float SetValue, float ActualValue, PID_TypeDef *PID)
{
	float PWM;                                  //增量

	PID->Ek = SetValue - ActualValue;           //计算当前偏差
	PID->LocSum += PID->Ek;                     //累计误差
	PWM = PID->Kp * (PID->Ek - PID->Ek1) + PID->Ki * PID->Ek + 
	PID->Kd * (PID->Ek - 2 * PID->Ek1 + PID->Ek2);                                          
																							//增量式PID控制器

	
	PID->Ek2 = PID->Ek1;
	PID->Ek1 = PID->Ek;
	
  return PWM;
}

// pid参数初始化函数
void PidInit(PID_TypeDef * pid)
{	
	pid->Kp     = 0;
	pid->Ki     = 0;
	pid->Kd     = 0;
	pid->Ek     = 0;
	pid->Ek1    = 0;
	pid->Ek2    = 0;
	pid->LocSum = 0;
}

