#include "sd5.h"

PID_TypeDef Servo_PID;
extern float yaw;

void Servo_init(void)
{
	pwm_init(Servo_PWM_TIM, Servo_PWM_CH1, 100, Servo_Middle_Num);
}

//100HZ
void Servo__Stright_PID_Init(void)
{
		PidInit(&Servo_PID);
	
	Servo_PID.Kp = 33;
	Servo_PID.Ki = 0;
	Servo_PID.Kd = 9;
}

void Servo_Turn_PID_Init(void)
{
	PidInit(&Servo_PID);
	
//	Servo_PID.Kp = 36.5;
	Servo_PID.Kp = 37.5;
	Servo_PID.Ki = 0;
	Servo_PID.Kd = 6;
}







